#!/usr/bin/env python3

import re
from functools import reduce

def get(data, field):
    keys = re.split('\.', field)
    ret = reduce(lambda val, key: val.get(key) if val else '', keys, data)
    return ret

if __name__ == '__main__':
    x = { 'a': { 'b': 'abc1234' } }

    test = get(x, 'a.b')
    # test = x['a']['b']
    print(test)
