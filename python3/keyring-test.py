#!/usr/bin/env python3

import keyring

mykey = 'MY-ADMIN'
username = 'opsman-for-platform-automation'
password = '1234'

keyring.set_password(mykey, username, password)
test = keyring.get_password(mykey, username)
print(test)
