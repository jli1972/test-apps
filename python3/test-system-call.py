#!/usr/bin/env python3

import os
import subprocess

if __name__ == '__main__':
    input = """1111111111111111
      222222222
      33333333
      555555555"""

    # method 1
    cmd = 'echo "{}" | grep --colour=auto --line-number {}'.format(input, '2222')
    os.system(cmd)

    # method 2
    cmd2 = 'echo "{}" | grep --colour=auto --line-number {}'.format(input, '333')
    output2 = subprocess.check_output(cmd2, shell=True, text=True)[:-1]
    print(output2)
