#!/usr/bin/env python3

from datetime import datetime
from dateutil import tz

# timezones
from_zone = tz.tzutc()
to_zone = tz.tzlocal()
# to_zone = tz.gettz('Asia/Hong_Kong')

# utc = datetime.utcnow()
utc_iso8601_timestamp = '2022-04-10T22:31:09.079Z'
utc = datetime.strptime(utc_iso8601_timestamp, "%Y-%m-%dT%H:%M:%S.%fZ")

# convert utc timestamp string to object
utc = utc.replace(tzinfo=from_zone)

# change timezone
local_timestamp = utc.astimezone(to_zone)
print('{} -> {}'.format(utc_iso8601_timestamp, local_timestamp))
