# How to map Azure Disks to Linux VM guest disks
1. lsscsi
  - Connect to the VM
  - sudo lsscsi
The first column listed will contain the LUN, the format is [Host:Channel:Target:LUN].

2. Listing block devices
  - Connect to the VM
  - sudo ls -l /sys/block/*/device
The last column listed will contain the LUN, the format is [Host:Channel:Target:LUN]

