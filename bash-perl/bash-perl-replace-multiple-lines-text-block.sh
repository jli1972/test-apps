#!/usr/bin/env bash

file='osconf-rc.yml.bak1'

# 1) To escape single quote in bash -- use '\'' for '
# 2) Expand the perl statement in multiple lines to enhance readability
perl -e 'my $input=`cat $ARGV[0]`; my $output = "";
  if ($input =~ m/text: '\''(.*)'\''/sm) {
    $output = "$1"; }; $output =~ s/\n/ /;
    $output =~ s/\s+/ /g;
    $output =~ s/\s+$//g;

    $input =~ s/( +)text: '\''(.*)'\''/$1text: |\n$1  $output/sm;
    print("$input");
' $file
