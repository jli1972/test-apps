#!/usr/bin/env bash

function test() {
  args="$1"
  resp="1234 $(date)"

  echo "input:  $args"
  echo "output: $resp"

  return 0
}

cmd="ls"
cmd="test '2222'"

base_dir=${0%%\/bin*}
if [[ "$base_dir" =~ ^\. ]]; then
  base_dir='.'
  echo "### base_dir = <$base_dir>"
else
  echo "### base_dir = <$base_dir>"
fi

eval "$cmd"
# echo "<$x>"

: '
This is a
multi line
comment
'
echo "1234"
