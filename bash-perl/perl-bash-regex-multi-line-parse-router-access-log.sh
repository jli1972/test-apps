#!/usr/bin/env bash

access_logs=$(cat <<EOF
router/780cadbd-0376-3a11-96c3-e7b108120c73: stdout | mesh.preprod.api.example.com - [2022-02-07T08:25:00.159494857Z] "GET /edgemicro_michefbff-ins-v1/v1/idn/chef/synchronize/policy?startDate=2022-02-06&endDate=2022-02-06 HTTP/1.1" 500 0 307 "-" "-" "10.223.62.202:38876" "10.223.56.48:61017" x_forwarded_for:"148.132.170.56, 52.220.42.97, 23.198.11.22, 10.107.253.163, 10.223.68.202" x_forwarded_proto:"https" vcap_request_id:"7bbae09a-2222-48e6-677b-03eeb68b2883" response_time:9.101615 gorouter_time:0.000178 app_id:"12186a9d-1381-4cfe-9fc2-91a2a74087ce" app_index:"9" instance_id:"0ff69c84-6745-45d2-5395-b972" x_cf_routererror:"-" x_b3_traceid:"5558052b0fd56ff41e202281c9b314cd" x_b3_spanid:"5558052b0fd56ff41e202281c9b314cd" x_b3_parentspanid:"-" b3:"5558052b0fd56ff41e202281c9b314cd-5558052b0fd56ff41e202281c9b314cd"
router/780cadbd-0376-3a11-96c3-e7b108120c73: stdout | eas.internal.mesh.api.example.com - [2022-02-07T08:25:08.147265025Z] "POST /common/document/service/v2/MMR/docs/documents HTTP/1.1" 500 4392963 136 "eClaims Microservices" "Java/1.8.0_312" "10.229.48.4:53146" "10.223.56.169:61023" x_forwarded_for:"10.223.56.170, 10.229.48.4" x_forwarded_proto:"https" vcap_request_id:"e0dcca8d-943f-2222-720b-802a8344be40" response_time:4.512778 gorouter_time:0.000172 app_id:"c0459339-244f-4b88-bb98-460edf6b8cdb" app_index:"0" instance_id:"dc8232ca-e3ce-42df-6315-ad15" x_cf_routererror:"-" x_b3_traceid:"44e351408c88685247b8b0052bae6300" x_b3_spanid:"44e351408c88685247b8b0052bae6300" x_b3_parentspanid:"-" b3:"44e351408c88685247b8b0052bae6300-44e351408c88685247b8b0052bae6300"
router/780cadbd-0376-3a11-96c3-e7b108120c73: stdout | common-service-eghl-adapter.apps.eas.vms.example.com - [2022-02-07T08:25:15.950124414Z] "POST /v2/usa/systems/movie/channels/AGENCY/payments/orders/866474_1h015qr HTTP/1.1" 500 389 53 "-" "Java/1.8.0_302" "10.223.62.197:50536" "10.223.56.152:61051" x_forwarded_for:"10.223.56.54, 10.223.62.197" x_forwarded_proto:"https" vcap_request_id:"c127d899-dddd-428f-7637-28b5f6cddee9" response_time:0.225085 gorouter_time:0.000186 app_id:"0a9fca11-56d0-4762-bf0d-d993694de878" app_index:"0" instance_id:"a5abdb90-c197-46e3-719a-f0e9" x_cf_routererror:"-" x_b3_traceid:"a7de2a2213bcdd84" x_b3_spanid:"705cdbebe4be0bc8" x_b3_parentspanid:"a7de2a2213bcdd84" b3:"a7de2a2213bcdd84-705cdbebe4be0bc8-0-a7de2a2213bcdd84"
router/780cadbd-0376-3a11-96c3-e7b108120c73: stdout | mesh.api.example.com - [2022-02-07T08:25:18.696623642Z] "GET /edgemicro_gfund_imworkflow_agah-tw-v1/v1/audit-service/v1/files/222222222222222222222222222222222222/metadata HTTP/1.1" 500 0 185 "-" "Jakarta Commons-HttpClient/3.1" "10.223.62.203:34524" "10.223.57.6:61019" x_forwarded_for:"142.76.220.11, 22.18.229.227, 10.107.253.133, 10.223.62.203" x_forwarded_proto:"https" vcap_request_id:"f5fdbfc3-31db-4318-4b3a-b29e8b897719" response_time:0.071739 gorouter_time:0.000153 app_id:"c7c3baa1-523d-4501-9f62-fd085213b08a" app_index:"8" instance_id:"fef64f4d-e110-43eb-5c2e-e54f" x_cf_routererror:"-" x_b3_traceid:"2d7d0d82dcd6a7d41bf38ed4a76ef7c7" x_b3_spanid:"2d7d0d82dcd6a7d41bf38ed4a76ef7c7" x_b3_parentspanid:"-" b3:"2d7d0d82dcd6a7d41bf38ed4a76ef7c7-2d7d0d82dcd6a7d41bf38ed4a76ef7c7"
EOF
)

echo "$access_logs" | perl -ne 'my $line = $_;
# if ($line =~ /^(?P<hostname>[a-zA-z0-9\.\-]+) - \[(?P<time>[^\]]+)\] "(?P<verb>[a-zA-Z]+) (?P<path>[^\" ]+) (?P<http_spec>[^\" ]+)" (?P<status>\d+) (?P<request_bytes_received>\d+) (?P<body_bytes_sent>\d+) "(?P<referer>[^\"]+)" "(?P<http_user_agent>[^\"]+)" "(?P<src>[^\"]+)" "(?P<dst>[^\"]+)" x_forwarded_for:"(?P<x_forwarded_for>[^\"]+)" x_forwarded_proto:"(?P<x_forwarded_proto>[^\"]+)" vcap_request_id:"(?P<vcap_request_id>[^\"]+)" response_time:(?P<response_time>[\d\.]+) gorouter_time:(?P<gorouter_time>[\d\.]+) app_id:"(?P<app_id>[^\"]+)" app_index:"(?P<app_index>[^\"]+)" x_cf_routererror:"(?P<x_cf_routererror>[^\"]+)" x_b3_traceid:"(?P<x_b3_traceid>[^\"]+)" x_b3_spanid:"(?P<x_b3_spanid>[^\"]+)" x_b3_parentspanid:"(?P<x_b3_parentspanid>[^\"]+)" b3:"(?P<b3>[^\"]+)"$/ {

  # $line =~ /^router\/[a-f0-9-]{36}: stdout [|] (?P<domain>[\w.-]+) - \[(?P<timestamp>[\w-.:]+)\] "(?P<method>\w+) (?P<url>[^\" ]+) (?P<protocol>[^\" ]+)" (?P<status>\d+) (?P<req_size>\d+) (?P<res_size>\d+) /i;

  $line =~ /
    ^router\/[a-f0-9-]{36}:
    \s
    stdout
    \s
    [|]
    \s
    (?P<domain>[\w.-]+)
    \s
    -
    \s
    \[(?P<timestamp>[\w-.:]+)\]
    \s
    "(?P<method>\w+)
    \s
    (?P<url>[^\" ]+)
    \s
    (?P<protocol>[^\" ]+)"
    \s
    (?P<status>\d+)
    \s
    (?P<req_size>\d+)
    \s
    (?P<res_size>\d+)
    \s
  /xi;

  if ($+{domain}) {
    printf("%s, %s, %s, %s, %s, <%s, %s, %s>\n", $+{domain}, $+{timestamp}, $+{method}, $+{url}, $+{protocol}, $+{status}, $+{req_size}, $+{res_size});
  }
'

# if ($line =~ /^router\/[a-f0-9-]{36}: stdout [|] ([\w-.].+) - \[([\w-.:]+)\] "(\w+) ([^\" ]+) ([^\" ]+)" (\d+) (\d+) (\d+) /i) {
#   print("$1, $2, $3, $4, $5, <$6, $7, $8>\n");
# }
# else {
#   # print($line);
# }
# bash b.sh | awk '{print $1}' | sort | uniq -c | sort -k 1 -n -t ' '
