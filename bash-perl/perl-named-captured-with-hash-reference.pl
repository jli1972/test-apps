#!/usr/bin/env perl

use Data::Dumper;

# input
my $variable = '12 abc';
print("Input = <$variable>\n");

# hash reference
my $matches = {};

# name captured:
# %{^CAPTURE}
# %LAST_PAREN_MATCH
# %+
# Similar to @+, the %+ hash allows access to the named capture buffers, should they exist, in the last successful match in the currently active dynamic scope.
# For example, $+{foo} is equivalent to $1 after the following match:
# 'foo' =~ /(?<foo>foo)/;
# The underlying behaviour of %+ is provided by the Tie::Hash::NamedCapture module.
# This variable was added in Perl v5.10.0. The %{^CAPTURE} alias was added in 5.25.7.
# This variable is read-only and dynamically-scoped.
@$matches{'count', 'name'} = $variable =~ /(?P<count>\d+) (?P<name>\w+)/;

print("output = " . Dumper($matches));

print "Count is $+{count}, name = <$+{name}>\n";

# %-
# Multiple named captured: %{^CAPTURE_ALL} or %-;
# To each capture group name found in the regular expression, it associates a reference to
# an array containing the list of values captured by all buffers with that name
# https://perldoc.perl.org/perlvar#%25-


