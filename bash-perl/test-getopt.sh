#!/bin/bash

aflag=no
bflag=no
cargument=none

# options may be followed by one colon to indicate they have a required argument
if ! options=$(/usr/local/opt/gnu-getopt/bin/getopt -o "abc:" -l "along,blong,clong:" -- "$@")
then
    echo "something went wrong, getopt will put out an error message for us"
    exit 1
else
	echo "else: <$options>"
fi

set -- $options
echo "1111"
while [ $# -gt 0 ]
do
    echo "---- $1"
    case $1 in
    -a|--along) aflag="yes" ;;
    -b|--blong) bflag="yes"; echo "### <$blfag>" ;;
    # for options with required arguments, an additional shift is required
    -c|--clong) cargument="$2" ; shift;;
    (--) shift; break;;
    (-*) echo "$0: error - unrecognized option $1" 1>&2; exit 1;;
    (*) echo "default"; break;;
    esac
    shift
done
echo "2222"
echo "aflag ($aflag), bflag ($bflag)"

