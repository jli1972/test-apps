#!/usr/bin/perl

my $timeout=3;

eval {
   local $SIG{ALRM} = sub { die "alarm\n" }; # NB: \n required
   alarm $timeout;
   # $nread = sysread SOCKET, $buffer, $size;
         sleep 30;
   alarm 0;
};
if ($@) {
   die unless $@ eq "alarm\n";   # propagate unexpected errors
      print("timeout in $timeout");
      # timed out
} else {
   # didn't
}
