#!/usr/bin/env bash

host=$1
port=$2

if [ -z "$host" ]; then
  host=$(ifconfig | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*' | grep -v '127.0.0.1')
  echo "Using default host $host"
fi

if [ -z "$port" ]; then
  port=80
  echo "Using default port $port"
fi

echo "### Start in 2 seconds ..."
tcpdump -v -nn -XX -s0 "(host $host)"
