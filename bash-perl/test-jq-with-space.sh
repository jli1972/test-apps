#!/usr/bin/env bash

json_data=$(cat << EOL
{
"gateways": [
    {
        "DHCP": "On",
        "External IPs": "46.244.46.66",
        "Firewall": "On",
        "NAT": "Off",
        "Name": "gateway",
        "Routed Networks": "photon, default-routed-network",
        "Selected": "*",
        "Syslog": "",
        "Uplinks": "d5p6v51-ext",
        "VPN": "Off"
    }
]
}
EOL
)

ip=$(echo "$json_data" | jq -r '.gateways[0]["External IPs"]')
echo "1) ip = $ip"

ip=$(echo "$json_data" | jq -r '.gateways[0]."External IPs"')
echo "2) ip = $ip"



