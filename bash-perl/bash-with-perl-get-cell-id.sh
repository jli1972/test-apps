#!/usr/bin/env bash

function usage() {
  usage_msg="Usage:\n\t### $0 <changelog path> [verbose]"
  sample_msg="Sample:\n\t$0 a.log -v"

  if [ -z "$input" ]; then
    echo "### Missing input file for opsman install changelog"
    echo -e "$usage_msg"
    exit 1
  fi
  if [[ "$input" == '-h' || "$input" == '--help' ]]; then
    echo -e "$usage_msg"
    echo -e "$sample_msg"
    exit 0
  fi
}

input=$1
verbose=$2
usage

# Sample logs:
# Task 540771 | 12:42:06 | Updating instance diego_cell: diego_cell/e8796705-211e-4d93-bcfe-11f9d2222222 (22) (00:12:08)
# Task 491140 | 22:33:54 | Updating instance rabbitmq-broker: rabbitmq-broker/73c02147-9de8-4a02-a5d6-111111111111 (0) (canary) (00:06:00)
# cat y | perl -ne 'my $name = ""; if ($_ =~ /Updating instance (?:\w+): ([^\/]+)\/([\w-]+) \((\d+)\) \(\d\d:/i) { $name = "$1/$2 $3\n"; print($name); }'

lines=$(cat $input | perl -ne 'my $name = ""; if ($_ =~ /Updating instance (?:[\w-]+): ([\w-]+)\/([\w-]+) \((\d+)\) (?:\(canary\) )?\(\d\d:/i) { $name = "$1/$2\n"; print($name); }')
echo "$lines"

if [ "$verbose" ]; then
  echo
  echo "--------------------------------------------------------------------------------"
  # num=$(echo "$lines" | wc -l | xargs)
  num=$(echo "$lines" | wc -l | xargs)
  echo "### <${num//[[:space:]]/}> instances were updated from changelog <$input>"
fi
